## Hash Flask Application

Para esse projeto que na verdade é um desafio bem legal de fazer, foi disponiblizado pela Hash uma aplicação em python utilizando flask, essa aplicação é um contador de requisições que utiliza redis para armazenar a quantidade dessas requisições.

Nesse repositório será descrito o pipeline de Continuous Integration CI onde será responsável por realizar o build e publicação na imagem no repositório imagens da aplicação, será informado as configurações dos manifestos do kubernetes que será responsável pela implantação no cluster criado no [projeto terraform](https://gitlab.com/hebersonaguiar/terraform-hash), também terá as informações do Dockerfile para criação da imagem. Esse repositório também vai está integrado com o [projeto Argo](https://gitlab.com/hebersonaguiar/hash-argocd) CD, reponsável pelo processo de Continuous Delivery (CD).

A estrutura do repositório é a seguinte:

```bash
hash-flask-app
   ├── app
   │   ├── app.py
   │   ├── __init__.py
   │   ├── __main__.py
   │   ├── README.md
   │   └── utils.py
   ├── deployment
   │   ├── dev
   │   │   ├── configmap.yaml
   │   │   ├── deployment.yaml
   │   │   ├── ingress.yaml
   │   │   ├── kustomization.yaml
   │   │   ├── namespace.yaml
   │   │   └── service.yaml
   │   └── prod
   │       ├── configmap.yaml
   │       ├── deployment.yaml
   │       ├── ingress.yaml
   │       ├── kustomization.yaml
   │       ├── namespace.yaml
   │       └── service.yaml
   ├── .gitlab-ci.yaml
   ├── Dockerfile
   ├── Makefile
   ├── Pipfile
   ├── Pipfile.lock
   ├── README.md
   ├── redis
   └── tests
       ├── conftest.py
       ├── __init__.py
       └── test_api.py
```

## Requisitos

Esse repositório faz parte de um projeto proposto pela Hash e para o funcionamento correto do pipeline de Continuous Integration CI utilizado nesse repositório é necessário:

- Cluster Kubernetes - para esse projeto foi utilizado o EKS criado com os dados do repositório [Terraform Hash](https://gitlab.com/hebersonaguiar/terraform-hash)
- Ambiente de Deploy - nesse projeto o CI/CD foram separados e com isso foi configurado para o CD o Argo CD dados no repositório [Hash Argo CD](https://gitlab.com/hebersonaguiar/hash-argocd)

## Dockerfile

O arquivo Dockerfile é o responsável por realizar a criação da imagem da aplicação, essa imagem utiliza como base uma imagem `python slim` que é uma imagem menor que nos auxilia na performance da aplicação e também no deploy, esse arquivo também contém o passo-a-passo para instalação da aplicação na imagem, com isso é executado a instalação do pip, a adição dos arquivos da aplicação que está dentro do diretório app e dos arquivos de modulos necessários para funcionamento da aplicação e também a execucção da aplicação, segue abaixo o arquivo completo:

```bash
cat > Dockerfile << EOF
FROM python:3.8.1-slim

RUN pip install pipenv==2020.11.15

WORKDIR /app

COPY Pipfile  .
COPY Pipfile.lock .
RUN pipenv install --system --deploy

COPY app/ .

CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0", "--port=8000" ]
EOF
```
## Kustomize

Para realizar o deploy da aplicação no cluster kubernetes é necessário um conjunto de arquivos de recursos específicos do kubernetes, nesse projeto serão utilizados:

- `namespace.yaml`  - arquivo responsável por criar o namespace que será implantado a aplicação.
- `deployment.yaml` - recurso responsável por implantar e gerenciar o container com a imagem da aplicação.
- `service.yaml`    - recurso responsável por cria o acesso aos pods que contém os containers da aplicação a nível interno do cluster.
- `configmap.yaml`  - recurso responsável por injetar variáveis não sensíveis na aplicação.
- `ingress.yaml`    - recurso responsável por criar o acesso externo para a aplicação.

O kustomize é um gerenciado dos manifestos dos recursos do kubernetes, com ele é possível adicionar, remover ou atualizar as configurações de forma simples e rápida, não é necessário nehuma instalação adicional no cluster, apenas o binário de execução no local onde será realizado o deploy, nesse projeto será no pipeline que será visto posteriormente.

Para que o kustomize possa gerenciar os manifestos é necessário a criação e configuração de um arquivo `yaml` chamado `kustomization.yaml`, com ele conseguimos informar quais arquivos serão utilizados para deploy da aplicação e também quais dados serão alterados, para esse projeto será alterado apenas a tag da imagem que será implantada, isso será realizado pelo pipeline que será executado, também nesse projeto será utilizadoos diretórios `development/dev` para o ambiente de desenvolvimento e `development/prod` para o ambiente de produção, segue abaixo o modelo utilizado nesse projeto:

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources: # recursos que serão utilizados para deploy da aplicação
- namespace.yaml
- deployment.yaml
- ingress.yaml
- service.yaml
- configmap.yaml
namespace: dev
images:
- name: registry.gitlab.com/hebersonaguiar/hash-flask-app # imagem que será utilizada
  newTag: "newTag" # tag que será alterada pelo pipeline para deploy da aplicação
```

* Aviso: a configuração do kustomization e os recursos utilizados acima serão utilizados para os dois ambientes do projeto, `dev` e `prod`, e nessa doc serão informadas quais pontos serão alterados para cada ambiente.

## Recursos Kubernetes

Como informado anteiormente alguns recursos do kubernetes são necessários para o deploy da aplicação no cluster, aqui será descrito as principais partes dos recursos utilizados para cada ambiente, como as alterações de cada ambiente são poucas essas partes serão descritas separadamente:

### Deployment

Env: dev / prod

```yaml
...
      containers:
      - image: registry.gitlab.com/hebersonaguiar/hash-flask-app:1.0 ## imagem utilizada com tag padrão, a tag será modificada durante o pipeline
        name: flask-app
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8000
          name: http
        envFrom:
        - configMapRef: # configmap com as variáveis não sensivéis para injetar no container/aplicação
            name: flask-app
        env:
        - name: AUTH_TOKEN # secret com variável sensível criada via linha de comando
          valueFrom:
            secretKeyRef:
              name: flask-app
              key: AUTH_TOKEN        
        resources: # configuração de utilização de recursos de memória e cpu
          requests:
            cpu: 10m
            memory: 50Mi
          limits:
            cpu: 30m
            memory: 100Mi
        livenessProbe: # configuração para verificar se o pod está apto para iniciar, caso ocorra algum erro o pod entra em estado de reinicialização na tentativa de correção
          failureThreshold: 3
          httpGet:
            path: /hc
            port: 8000 # porta do container
          initialDelaySeconds: 30
          periodSeconds: 20
          timeoutSeconds: 5
        readinessProbe: # o readiness assegura que o a aplicação esteja em funcionamento porém ainda sem requisições até finalizar suas ações de checagem
          failureThreshold: 3
          httpGet:
            path: /hc
            port: 8000 # porta do container
          periodSeconds: 20
          timeoutSeconds: 5
          successThreshold: 3
```

### Service

Env: dev / prod

```yaml
...
spec:
  ports:
  - port: 80 # porta de acesso ao serviço
    name: http
    protocol: TCP
    targetPort: 8000 # porta de acesso à aplicação
...
```

### Configmap

Env: dev / prod

```yaml
...
data:
  REDIS_HOST: redis.redis.svc.cluster.local ## apontamento para o redis (a implantação será informada posteriormente)
...
```

### Namespace

Env: dev

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```

Env: prod

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: prod
```

### Ingress

Env: dev

```yaml
...
  annotations: 
    kubernetes.io/ingress.class: nginx # annotation necessário para utilização do Nginx Controller
spec:
  rules:
  - host: flask-app-dev.hebersonaguiar.com.br # apontamento dns criado pelo projeto do terraform
    http:
      paths:
      - backend:
          serviceName: flask-app-svc # serviço de acesso aos containers
          servicePort: http
...
```

Env: prod

```yaml
...
  annotations: 
    kubernetes.io/ingress.class: nginx # annotation necessário para utilização do Nginx Controller
spec:
  rules:
  - host: flask-app.hebersonaguiar.com.br # apontamento dns criado pelo projeto do terraform
    http:
      paths:
      - backend:
          serviceName: flask-app-svc # serviço de acesso aos containers
          servicePort: http
...
```

### Secret

O secret é similar ao configmap, injeta vairáveis dentro do container no momento do deploy, entretanto ele é comumente usado para dasos sensíveis, como por exemplo dados de credenciais de banco de dados, certificados, etc. Nesse projeto será criado uma variável com um token que servirá de chave de acesso no momento da requisição. Como o secret injeta dados sensíveis para a aplicação, não é seguro colocar esses dados em um arquivo do tipo secret, o ideal é utilizar um "cofre se senha" que possa gerenciar esses dados junto ao cluster, entrentanto para esse projeto será utilizado o recurso do próprio kubernetes e executado via linha de comando, é importante que seja criado antes de ser realizado o deploy das aplicações, segue abaixo:

```bash
kubectl create secret generic flask-app --from-literal=AUTH_TOKEN=123456 -n dev
```

```bash
kubectl create secret generic flask-app --from-literal=AUTH_TOKEN=123456 -n prod
```

## Redis

Conforme informado no inicio dessa documentação, essa aplicação necessita do redis para salvar as contagens das requisições, para esse projeto srá utilizado apenas um para os dois ambiente, mas é altamente recomendável que seja separado por ambiente `redis-dev` e `redis-prod` e também configurado com alta disponibilidade e resiliência.

Nesse projeto o redis terá configurações simples e rápida e sem a necessidade de um repositório destinado para ele, dessa forma suas configurações estão na pasta `redis`

### Namespace

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: redis
```


### Deployment

```yaml
...
    spec:
      containers:
      - image: redis:6.2.3-alpine # versão do redis utilizada no projeto inicial
        name: redis
        imagePullPolicy: Always
        ports:
        - containerPort: 6379
...
```

### Service

```yaml
...
spec:
  ports:
  - port: 6379 
    name: redis
    protocol: TCP
    targetPort: 6379
...
```

Para aplicar as configurações, execute os comandos abaixo:

```bash
kubectl apply -f redis/namespace.yaml
```

```bash
kubectl apply -f redis/deployment.yaml -n redis
```

```bash
kubectl apply -f redis/service.yaml -n redis
```

Realizado as configurações do redis, pode-se então configurar o pipeline para deploy da aplicação.


## Pipeline

O próximo passo agora é a criaçã do pipeline para deploy da aplicação, no qual consistirá em quando o desenvolvedor realizar o commit no repositório automaticamente será executado o pipeline para deploy nos ambientes.

Como está sendo utilizado o Gitlab o pipeline será definido pelo .gitlab-ci.yaml e esse arquivo deve está na raiz do projeto. Outra configuração necessária no repositório também é a criação e configuração do token para integração com o Argo CD, para isso acesso o [projeto do Argo CD](https://gitlab.com/hebersonaguiar/hash-argocd#configura%C3%A7%C3%A3o-do-gitlab) para realizar essas configurações.

### .gitlab-ci.yaml

A primeira parte a ser configurada no pipeline é declarar os stages que serão executados, nesse caso será configurado os stages de `build-and-push` responsável por realizar o build da imagem e enviar para o container registry do repositório, `deploy-dev` stage responsável por realizar o deploy automático da aplicação no ambiente de desenvolvimento e  `deploy-prod` stage responsável por realizar o deploy manual da aplicação no ambiente de produção após o deploy em ambiente de desenvolvimento.

```yaml
stages:
  - build-and-push
  - deploy-dev
  - deploy-prod
```

Declarado os stages necessários, pode-se então configurar cada um, o primeiro a ser configurado deve se o stage `build-and-push`, basicamente esse stage realiza o build e push da imagem para o container registry utilizando as variáveis do próprio gitlab. A imagem que será utilizada é a `gcr.io/kaniko-project/executor:debug` que já está configuarada com para realizar essas ações, segue abaixo a definição do stage:

```yaml
build-and-push:
  stage: build-and-push
  image:
    name: gcr.io/kaniko-project/executor:debug  # imagem com as configurações necessárias para build e push
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile ./Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID # script que utiliza variáveis do gitlab para build da imagem e push para o registry
  only:
    - master # indica que esse stage poderá ser executado apenas na branch master desse repositório
```

Após a criação do stage `build-and-push` criado pode-se então criar os stages de `deploy-dev` e `deploy-prod`, obrigatoriamente o deploy em ambiente de produção só será realizado após o deploy em ambiente de desenvolvimento, dessa forma segue abaixo a configuração do stage de deploy em ambiente de desenvolvimento:

```yaml
deploy-dev:
  stage: deploy-dev
  image: alpine:3.8 # imagem utilizada para realizar o deploy
  before_script:
    - apk add --no-cache git curl bash coreutils # pacotes necessários para realização dos comandos abaixo e consequentemente o deploy
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash  # download do script para instalação do kustomize
    - mv kustomize /usr/local/bin/ # configuração do binário do kustomize par utilização
    - git remote set-url origin https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/hebersonaguiar/hash-flask-app.git # configuração de repositóiro para downalod 
    - git config --global user.email "hebersonaguiar@gmail.com" # configuração do usuário para execução dos comandos git necessários
    - git config --global user.name "hebersonaguiar" # configuração do email para execução dos comandos git necessários
  script:
    - git checkout -B master # realiza o checkout em na branch master
    - cd deployment/dev  # acessa o diretório dos recursos do kubernetes para ambiente de desenvolvimento
    - kustomize edit set image $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID # altera a imagem que será utilizada para deploy em ambiente de desenvolvimeto (a mesma criada no stage de build-and-push)
    - cat kustomization.yaml  # comando apenas para visualização das alterações do kustomize com a nova imagem
    - git commit -am '[skip ci] DEV image update' # devido a alteração do arquivo kustomization é necessário um commit com a nova alteração, para que não seja executado o pipeline novamente é utilizado a tag [skip ci]
    - git push origin master # envio das mudanças para o repositório.
  only:
    - master # indica que esse stage poderá ser executado apenas na branch master desse repositório
```

Configuração do stage para deploy em ambiente de desenvolvimento realizado, pode-se então realizar a criação do stage de deploy em ambiente de produção, as configuração são similares, segue abaixo:

```yaml
deploy-prod:
  stage: deploy-prod
  image: alpine:3.8 # imagem utilizada para realizar o deploy
  before_script:
    - apk add --no-cache git curl bash coreutils # pacotes necessários para realização dos comandos abaixo e consequentemente o deploy
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash # download do script para instalação do kustomize
    - mv kustomize /usr/local/bin/ # configuração do binário do kustomize par utilização
    - git remote set-url origin https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/hebersonaguiar/hash-flask-app.git # configuração de repositóiro para downalod
    - git config --global user.email "hebersonaguiar@gmail.com" # configuração do usuário para execução dos comandos git necessários
    - git config --global user.name "hebersonaguiar" # configuração do email para execução dos comandos git necessários
  script:
    - git checkout -B master # realiza o checkout em na branch master
    - git pull origin master # devido o commit e push realizado no stage anterior é necessário realizar o pull do repositório com as novas mudanças
    - cd deployment/prod # acessa o diretório dos recursos do kubernetes para ambiente de desenvolvimento
    - kustomize edit set image $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID # altera a imagem que será utilizada para deploy em ambiente de desenvolvimeto (a mesma criada no stage de build-and-push)
    - cat kustomization.yaml # comando apenas para visualização das alterações do kustomize com a nova imagem
    - git commit -am '[skip ci] PROD image update' # devido a alteração do arquivo kustomization é necessário um commit com a nova alteração, para que não seja executado o pipeline novamente é utilizado a tag [skip ci]
    - git push origin master # envio das mudanças para o repositório.
  only:
    - master # indica que esse stage poderá ser executado apenas na branch master desse repositório
  when: manual # indica que esse stage só será executado manualmente.
```

Após as configurações acima o pipeline está pronto para o uso, com isso basta realizar o commit no repositório que o pipeline é executado e o deploy realizado no cluster kubernetes com apoio do Argo CD.

Após o deploy das aplicações é possível realizar a consulta e verificar o funcionamento da aplicação nos ambientes, para isso execute os comandos abaixo a partir de um terminal utilizando o comando `curl`

Env: prod

```bash
curl -H "Authorization: Token 123456" flask-app.hebersonaguiar.com.br
```

Env: dev

```bash
curl -H "Authorization: Token 123456" flask-app-dev.hebersonaguiar.com.br
```

## Videos

Para poder visualizar a execução da implantação do Redis clique [aqui](https://1drv.ms/v/s!Ar9b9qXQcSfSkUzi1lpnngIsbHO7?e=0R4sLV) e faça o download do video.

Para poder visualizar a execução da implantação do pipeline e deploy da aplicação nos ambientes clique [aqui](https://1drv.ms/v/s!Ar9b9qXQcSfSkU5BTZIkFZDKHncz?e=qo0XdM) e faça o download do video.